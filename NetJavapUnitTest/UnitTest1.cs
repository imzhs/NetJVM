﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetJavap;
using NetJavap.AttributeInfos;
using NetJavap.JVM;

namespace NetJavapUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            for (int i = 0; i < 100; i++)
            {
                JavaClass entry = JVMClassLoader.LoadClass("Main");
                CodeAttributeInfo codeInfo = entry.Methods[1].GetAttribute<CodeAttributeInfo>();
                JVMCodeRunner runner = new JVMCodeRunner(codeInfo);
                JVMCodeRunner.OnNativeInvokeNeed += runner_OnNativeInvokeNeed;
                while (!runner.ExecuteSetp()) ;
            }
        }

        void runner_OnNativeInvokeNeed(object sender, NativeInvokeEventArgs e)
        {
            switch (e.Method.Name.Value)
            {
                case "sin":
                    e.ReturnValue = Math.Sin((double)e.Parameters[0]);
                    break;
                default:
                    break;
            }
        }
    }
}
