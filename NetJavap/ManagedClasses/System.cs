﻿using NetJavap.JVM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ManagedClasses
{
    public class System : ManagedJavaClass
    {
        public override object Invoke(MethodInfo method, JVMStackFrame stackFrame)
        {
            throw new NotImplementedException();
        }
        public override void InitStatic()
        {
            JavaClass outStream = JVMClassLoader.LoadClass(@"java/io/PrintStream");
            JavaClass inStream = JVMClassLoader.LoadClass(@"java/io/InputStream");
            this.SetStaticValue("out", JavaObject.GetStaticInstance(outStream));
            this.SetStaticValue("in", JavaObject.GetStaticInstance(inStream));
        }

        public System(BinaryReader reader) : base(reader) { }

        
    }
}
