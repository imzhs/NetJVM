﻿using NetJavap.JVM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ManagedClasses
{
    public class PrintStream : ManagedJavaClass
    {
        public override object Invoke(MethodInfo method, JVMStackFrame stackFrame)
        {
            switch (method.Name.Value)
            {
                case "println":
                    Object obj;
                    if (stackFrame.LocalVariableTable.TryGetValue(1, out obj))
                    {
                        Console.WriteLine(stackFrame.LocalVariableTable[1]);
                    }
                    else
                    {
                        Console.WriteLine();
                    }
                    break;
                case "print":
                    Object printObj = stackFrame.LocalVariableTable[1];
                    if (method.Parameters[0].Name == "C")
                    {
                        printObj = Convert.ToChar(printObj);
                    }
                    if (method.Parameters[0].Name == "java/lang/String")
                    {
                        JavaObject javaObj = printObj as JavaObject;

                    }
                    Console.Write(printObj);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return null;
        }

        public PrintStream(BinaryReader reader) : base(reader) { }

        public override void InitStatic()
        {

        }
    }
}
