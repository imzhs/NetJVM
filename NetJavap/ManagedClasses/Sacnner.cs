﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ManagedClasses
{
    public class Sacnner : ManagedJavaClass
    {
        private JavaClass printStreamCls;

        public override object Invoke(MethodInfo method, JVM.JVMStackFrame stackFrame)
        {
            switch (method.Name.Value)
            {
                case "<init>":
                    this.printStreamCls = stackFrame.LocalVariableTable[1] as JavaClass;
                    return null;
                case "nextInt":
                    return Int32.Parse(Console.ReadLine());
                default:
                    throw new NotImplementedException();
            }
        }

        public override void InitStatic()
        {
        }

        public Sacnner(BinaryReader reader):base(reader)
        {
        }
    }
}
