﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantClassInfo : ConstantInfo
    {
        private UInt16 _nameIndex;

        private ConstantUtf8Info _name = null;
        private JavaClass _class;
        private TypeInfo _typeInfo;

        public TypeInfo TypeInfo
        {
            get {
                if (this._name == null)
                {
                    this._name = this.ParentClass.ConstantPool[this._nameIndex] as ConstantUtf8Info;
                }
                if (this._typeInfo == null)
                {
                    this._typeInfo = ArrayBuilder.CreateTypeInfo(this._name.Value);
                }
                return this._typeInfo;
            }
        }
        public JavaClass Class
        {
            get
            {
                if (this._class == null)
                {
                    this._class = JVM.JVMClassLoader.LoadClass(this.TypeInfo.Name);
                }
                return this._class;
            }
        }

        public ConstantClassInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._nameIndex = reader.ReadUInt16BE();
        }

        public override object GetValue()
        {
            return this.Class;
        }
        public override string ToString()
        {
            return this.TypeInfo.Name;
        }
    }
}
