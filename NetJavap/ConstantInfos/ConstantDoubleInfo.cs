﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantDoubleInfo : ConstantInfo
    {
        private Double _value;

        public Double Value { get { return _value; } }

        public ConstantDoubleInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._value = reader.ReadDoubleBE();
        }

        public override object GetValue()
        {
            return this.Value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
