﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantLongInfo : ConstantInfo
    {
        private Int64 _value;

        public Int64 Value { get { return _value; } }

        public ConstantLongInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._value = reader.ReadInt64BE();
        }
        public override object GetValue()
        {
            return this.Value;
        }
        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
