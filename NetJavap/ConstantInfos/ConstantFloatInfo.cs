﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantFloatInfo : ConstantInfo
    {
        private Single _value;

        public Single Value { get { return _value; } }

        public ConstantFloatInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._value = reader.ReadSingleBE();
        }

        public override object GetValue()
        {
            return this.Value;
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
