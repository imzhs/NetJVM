﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantNameAndTypeInfo : ConstantInfo
    {
        private UInt16 _nameIndex;
        private UInt16 _descriptorIndex;

        private ConstantUtf8Info _name;
        private ConstantUtf8Info _descriptor;

        public ConstantUtf8Info Name {
            get
            {
                if (_name == null)
                {
                    _name = this.ParentClass.ConstantPool[this._nameIndex] as ConstantUtf8Info;
                }
                return _name;
            }
        }

        public ConstantUtf8Info Descriptor
        {
            get
            {
                if (_descriptor == null)
                {
                    _descriptor = this.ParentClass.ConstantPool[this._descriptorIndex] as ConstantUtf8Info;
                }
                return _descriptor;
            }
        }

        public ConstantNameAndTypeInfo(BinaryReader reader,JavaClass cls)
            : base(reader,cls)
        {
            this._nameIndex = reader.ReadUInt16BE();
            this._descriptorIndex = reader.ReadUInt16BE();
        }

        public override string ToString()
        {
            return Name.Value + ":" + Descriptor.Value;
        }
    }
}
