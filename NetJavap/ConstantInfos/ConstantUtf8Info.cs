﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantUtf8Info : ConstantInfo
    {
        private UInt16 _length;
        private byte[] _bytes;

        private String _stringValue = null;

        public string Value
        {
            get
            {
                if (_stringValue == null)
                {
                    _stringValue = Encoding.UTF8.GetString(_bytes);
                }
                return _stringValue;
            }
        }

        public ConstantUtf8Info(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._length = reader.ReadUInt16BE();
            this._bytes = reader.ReadBytesRequired(this._length);
        }

        public override object GetValue()
        {
            return this.Value;
        }

        public override string ToString()
        {
            return this.Value;
        }
    }
}
