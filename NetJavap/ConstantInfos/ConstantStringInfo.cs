﻿using NetJavap.JVM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantStringInfo : ConstantInfo
    {
        private UInt16 _stringIndex;

        private ConstantUtf8Info _value;
        private JavaObject _instance;

        public ConstantUtf8Info Value
        {
            get
            {
                if (_value == null)
                {
                    _value = this.ParentClass.ConstantPool[this._stringIndex] as ConstantUtf8Info;
                }
                return _value;
            }
        }

        public ConstantStringInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._stringIndex = reader.ReadUInt16BE();
        }

        public override object GetValue()
        {
            if (this._instance == null)
            {
                this._instance = Utils.CreateStringInstance(this.Value.Value);
            }
            return this._instance;
        }

        public override string ToString()
        {
            return this.Value.Value;
        }
    }
}
