﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantInterfaceMethodRefInfo : ConstantMemberInfo
    {
        public ConstantInterfaceMethodRefInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {

        }
    }
}
