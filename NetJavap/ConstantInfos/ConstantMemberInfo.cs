﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantMemberInfo : ConstantInfo
    {
        private UInt16 _classIndex;
        private UInt16 _nameAndTypeIndex;

        private ConstantClassInfo _classInfo;
        private ConstantNameAndTypeInfo _nameAndTypeInfo;

        public ConstantClassInfo ClassInfo { 
            get {
                if(_classInfo == null)
                {
                    _classInfo = this.ParentClass.ConstantPool[this._classIndex] as ConstantClassInfo;
                }
                return _classInfo;
            } 
        }

        public ConstantNameAndTypeInfo NameAndKeyInfo
        {
            get
            {
                if (_nameAndTypeInfo == null)
                {
                    _nameAndTypeInfo = this.ParentClass.ConstantPool[this._nameAndTypeIndex] as ConstantNameAndTypeInfo;
                }
                return _nameAndTypeInfo;
            }
        }

        public ConstantMemberInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._classIndex = reader.ReadUInt16BE();
            this._nameAndTypeIndex = reader.ReadUInt16BE();
        }

        public override object GetValue()
        {
            return this;
        }

        public override string ToString()
        {
            return this.ClassInfo.ToString() + "." + this.NameAndKeyInfo.ToString();
        }
    }
}
