﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.ConstantInfos
{
    public class ConstantIntegerInfo : ConstantInfo
    {
        Int32 _value;

        public Int32 Value { get { return _value; } }

        public ConstantIntegerInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
            this._value = reader.ReadInt32BE();
        }

        public override object GetValue()
        {
            return this.Value;
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
