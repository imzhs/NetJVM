﻿using NetJavap.JVM;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetJavap;

namespace NetJavap.ConstantInfos
{
    public class ConstantFieldRefInfo : ConstantMemberInfo
    {
        public ConstantFieldRefInfo(BinaryReader reader, JavaClass cls)
            : base(reader, cls)
        {
        }

        public override Object GetValue()
        {
            JavaClass cls = JVMClassLoader.LoadClass(this.ClassInfo.TypeInfo.Name);
            return cls.GetStaticValue(this.NameAndKeyInfo.Name.Value);
        }

        public override void SetValue(Object value)
        {
            JavaClass cls = JVMClassLoader.LoadClass(this.ClassInfo.TypeInfo.Name);
            cls.SetStaticValue(this.NameAndKeyInfo.Name.Value, value);
        }
    }
}
