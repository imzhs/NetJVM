﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.AttributeInfos
{
    public class StackMapTableAttributeInfo : CustomAttributeInfo
    {
        public StackMapTableAttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {

        }
    }
}
