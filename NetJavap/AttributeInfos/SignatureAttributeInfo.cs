﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetJavap.ConstantInfos;

namespace NetJavap.AttributeInfos
{
    public class SignatureAttributeInfo : AttributeInfo
    {
        private UInt16 _signatureIndex;

        private ConstantUtf8Info _signature;

        public ConstantUtf8Info Signatrue
        {
            get
            {
                if (this._signature == null)
                {
                    this._signature = this.ParentClass.ConstantPool[this._signatureIndex] as ConstantUtf8Info;
                }
                return this._signature;
            }
        }

        public SignatureAttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {
            this._signatureIndex = reader.ReadUInt16BE();
        }
    }
}
