﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.AttributeInfos
{
    public class LineNumberTableAttribbuteInfo : AttributeInfo
    {
        private UInt16 _lineNumberTableLength;
        private LineNumberInfo[] _lineNumberTable;

        public LineNumberInfo[] LineNumbers { get { return this._lineNumberTable; } }

        public LineNumberTableAttribbuteInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {
            this._lineNumberTableLength = reader.ReadUInt16BE();
            this._lineNumberTable = ArrayBuilder.BuildArray(() => new LineNumberInfo(reader), this._lineNumberTableLength);
        }
    }
}
