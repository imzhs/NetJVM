﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.AttributeInfos
{
    public class ConstantValueAttributeInfo : AttributeInfo
    {
        private UInt16 _constantvalueIndex;

        private ConstantInfo _value;

        public ConstantInfo Value
        {
            get
            {
                if (this._value == null)
                {
                    this._value = this.ParentClass.ConstantPool[this._constantvalueIndex];
                }
                return this._value;
            }
        }

        public ConstantValueAttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {
            this._constantvalueIndex = reader.ReadUInt16BE();
        }

        public override string ToString()
        {
            return base.ToString() + " : " + this.Value.ToString();
        }
    }
}
