﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.AttributeInfos
{
    public class LocalVariableTableAttributeInfo : AttributeInfo
    {
        private UInt16 _localVariableTableLength;
        private LocalVariableInfo[] _localVariableTable;

        public LocalVariableInfo[] LocalVariables { get { return this._localVariableTable; } }

        public LocalVariableTableAttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {
            this._localVariableTableLength = reader.ReadUInt16BE();
            this._localVariableTable = ArrayBuilder.BuildArray(() => new LocalVariableInfo(reader, cls), this._localVariableTableLength);
        }
    }
}
