﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.AttributeInfos
{
    public class CustomAttributeInfo : AttributeInfo
    {
        private byte[] _info;

        public Byte[] Info { get { return this._info; } }

        public CustomAttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {
            this._info = reader.ReadBytesRequired(base._attributeLength);
        }
    }
}
