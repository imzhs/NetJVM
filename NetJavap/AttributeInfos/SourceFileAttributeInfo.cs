﻿using NetJavap.ConstantInfos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.AttributeInfos
{
    public class SourceFileAttributeInfo : AttributeInfo
    {
        private UInt16 _sourceFileIndex;

        private ConstantUtf8Info _sourceFile;

        public ConstantUtf8Info SourceFile {
            get
            {
                if (this._sourceFile == null)
                {
                    this._sourceFile = this.ParentClass.ConstantPool[this._sourceFileIndex] as ConstantUtf8Info;
                }
                return this._sourceFile;
            }
        }

        public SourceFileAttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {
            this._sourceFileIndex = reader.ReadUInt16BE();
        }

        public override string ToString()
        {
            return base.ToString() + " : " + this.SourceFile.Value;
        }
    }
}
