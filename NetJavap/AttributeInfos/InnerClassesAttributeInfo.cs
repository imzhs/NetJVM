﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.AttributeInfos
{
    public class InnerClassesAttributeInfo : AttributeInfo
    {
        private UInt16 _numberOfClasses;
        private InnerClassInfo[] _innerClasses;

        public InnerClassInfo[] InnerClasses { get { return this._innerClasses; } }

        public InnerClassesAttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(reader, cls, defineMember)
        {
            this._numberOfClasses = reader.ReadUInt16BE();
            this._innerClasses = ArrayBuilder.BuildArray(() => new InnerClassInfo(reader), this._numberOfClasses);
        }
    }
}
