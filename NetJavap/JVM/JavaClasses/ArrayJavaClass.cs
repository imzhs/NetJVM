﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.JVM
{
    public class ArrayJavaClass : JavaClass
    {
        private static Dictionary<Object, Dictionary<Int32, ArrayJavaClass>> _cachedArrayClasses = new Dictionary<Object, Dictionary<Int32, ArrayJavaClass>>();

        private Int32 _length;
        private Object _type;

        public Int32 Length { get { return this._length; } }
        public Object Type { get { return this._type; } }

        public static ArrayJavaClass GetArrayClass(Object type, Int32 length)
        {
            Dictionary<Int32, ArrayJavaClass> cachedArray;
            if (!_cachedArrayClasses.TryGetValue(type, out cachedArray))
            {
                cachedArray = new Dictionary<Int32, ArrayJavaClass>();
                _cachedArrayClasses[type] = cachedArray;
            }
            ArrayJavaClass cachedArrayClass;
            if (!cachedArray.TryGetValue(length,out cachedArrayClass))
            {
                cachedArrayClass = new ArrayJavaClass(type, length);
                cachedArray[length] = cachedArrayClass;
            }
            return cachedArrayClass;
        }

        public ArrayJavaClass(Object type, Int32 length)
            : base(null)
        {
            this._length = length;
            this._type = type;
            _cachedArrayClasses[type][length] = this;
        }
    }
}
