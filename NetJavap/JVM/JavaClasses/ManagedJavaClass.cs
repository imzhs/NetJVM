﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetJavap.JVM;
using NetJavap.ConstantInfos;

namespace NetJavap
{
    public abstract class ManagedJavaClass : JavaClass
    {
        public abstract object Invoke(MethodInfo method,JVMStackFrame stackFrame);
        public abstract void InitStatic();
        public ManagedJavaClass(BinaryReader reader) : base(reader) {
        }
    }
}
