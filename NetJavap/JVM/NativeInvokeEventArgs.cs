﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.JVM
{
    public class NativeInvokeEventArgs : EventArgs
    {
        private MethodInfo _method;
        private Dictionary<ushort, Object> _parameters;
        private Object _bindObj;

        public MethodInfo Method { get { return this._method; } }
        public Dictionary<ushort, Object> Parameters { get { return this._parameters; } }
        public Object ReturnValue { get; set; }
        public Object BindInstance { get { return this._bindObj; } }

        public NativeInvokeEventArgs(Dictionary<ushort, Object> parameters, MethodInfo method,Object bindObj = null)
        {
            this._method = method;
            this._parameters = parameters;
            this._bindObj = bindObj;
        }
    }
}
