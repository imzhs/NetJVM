﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap.JVM
{
    public class ArrayJavaObject : JavaObject
    {
        private Object[] _elements;
        public Object[] Elements { get { return this._elements; } }

        public ArrayJavaObject(ArrayJavaClass cls):base(cls)
        {
            this._elements = new Object[cls.Length];
        }
    }
}
