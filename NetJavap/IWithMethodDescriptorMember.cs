﻿using NetJavap.ConstantInfos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public interface IWithMethodDescriptorMember
    {
        ConstantUtf8Info Descriptor { get; }
        TypeInfo ReturnType { get; }
        TypeInfo[] Parameters { get; }
    }
}
