﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class LineNumberInfo
    {
        private UInt16 _startPC;
        private UInt16 _lineNumber;

        public LineNumberInfo(BinaryReader reader)
        {
            this._startPC = reader.ReadUInt16BE();
            this._lineNumber = reader.ReadUInt16BE();
        }
    }
}
