﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class InnerClassInfo
    {
        private UInt16 _innerClassInfoIndex;
        private UInt16 _outerClassInfoIndex;
        private UInt16 _innerNameIndex;
        private AccessFlags _innerNameAccessFlags;

        public InnerClassInfo(BinaryReader reader)
        {
            this._innerClassInfoIndex = reader.ReadUInt16BE();
            this._outerClassInfoIndex = reader.ReadUInt16BE();
            this._innerNameIndex = reader.ReadUInt16BE();
            this._innerNameAccessFlags = (AccessFlags)reader.ReadUInt16BE();
        }

    }
}
