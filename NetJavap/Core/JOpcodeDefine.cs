﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class JOpcodeDefine
    {
        public string Mnemonic { get;private set; }
        public Byte OpCode { get; private set; }
        public Byte Length { get; private set; }
        public OpCodeExecuteAction ExecuteAction { get;private set; }
        public JOpcodeDefine(Byte opcode,Byte len,string mnemonic,OpCodeExecuteAction executeAct = null)
        {
            this.OpCode = opcode;
            this.Length = len;
            this.Mnemonic = mnemonic;
            this.ExecuteAction = executeAct;
        }
    }
}
