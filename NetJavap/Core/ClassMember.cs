﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class ClassMember
    {
        public JavaClass ParentClass { get;private set; }

        public ClassMember(JavaClass cls)
        {
            this.ParentClass = cls;
        }
    }
}
