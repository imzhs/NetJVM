﻿using NetJavap.ConstantInfos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class AttributeInfo : ClassMember
    {
        private UInt16 _attributeNameIndex;
        protected UInt32 _attributeLength; 
        
        private ConstantUtf8Info _name;
        public ConstantUtf8Info Name
        {
            get
            {
                if (this._name == null)
                {
                    this._name = this.ParentClass.ConstantPool[this._attributeNameIndex] as ConstantUtf8Info;
                }
                return this._name;
            }
        }
        public ClassMember DefineMember { get; internal set; }
        public AttributeInfo(BinaryReader reader, JavaClass cls, ClassMember defineMember)
            : base(cls)
        {
            this._attributeNameIndex = reader.ReadUInt16BE();
            this._attributeLength = reader.ReadUInt32BE();

            this.DefineMember = defineMember;
        }
        public override string ToString()
        {
            return this.Name.Value;
        }
    }
}
