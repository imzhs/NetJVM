﻿using NetJavap.ConstantInfos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class ExceptionInfo : ClassMember
    {
        private UInt16 _startPC;
        private UInt16 _endPC;
        private UInt16 _handlerPC;
        private UInt16 _catchType;

        private ConstantClassInfo _catchTypeClass;

        public UInt16 StartPC { get { return this._startPC;} }
        public UInt16 EndPC { get { return this._endPC; } }
        public UInt16 HandlerPC { get { return this._handlerPC; } }
        public ConstantClassInfo CatchType { 
            get {
                if (this._catchTypeClass == null)
                {
                    this._catchTypeClass = this.ParentClass.ConstantPool[this._catchType] as ConstantClassInfo;
                }
                return this._catchTypeClass;
            } 
        }
        public ExceptionInfo(BinaryReader reader, JavaClass cls)
            : base(cls)
        {
            this._startPC = reader.ReadUInt16BE();
            this._endPC = reader.ReadUInt16BE();
            this._handlerPC = reader.ReadUInt16BE();
            this._catchType = reader.ReadUInt16BE();
        }

        public override string ToString()
        {
            return "throw " + this._catchTypeClass.TypeInfo.ToString();
        }
    }
}
