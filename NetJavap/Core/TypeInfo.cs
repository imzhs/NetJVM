﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class TypeInfo
    {
        private String _name;
        private int _arrayLength;

        public String Name { get { return this._name; } }
        public int ArrayLength { get { return this._arrayLength; } }

        public TypeInfo(String name,int arrLength)
        {
            this._name = name;
            this._arrayLength = arrLength;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(this.Name.Length + this.ArrayLength * 2);
            sb.Append(this.Name);
            for (int i = 0; i < this._arrayLength; i++)
			{
                sb.Append("[]");
			}
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TypeInfo))
            {
                return base.Equals(obj);
            }
            TypeInfo einfo = obj as TypeInfo;
            return this.Name == einfo.Name && this.ArrayLength == einfo.ArrayLength;
        }
    }
}
