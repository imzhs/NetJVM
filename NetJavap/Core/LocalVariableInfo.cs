﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetJavap.ConstantInfos;

namespace NetJavap
{
    public class LocalVariableInfo : ClassMember
    {
        private UInt16 _startPC;
        private UInt16 _length;
        private UInt16 _nameIndex;
        private UInt16 _descriptorIndex;
        private UInt16 _index;

        private ConstantUtf8Info _name;
        private ConstantUtf8Info _description;

        public UInt16 StartPC { get { return this._startPC; } }
        public UInt16 Length { get { return this._length; } }
        public ConstantUtf8Info Name { 
            get {
                if (_name == null)
                {
                    this._name = this.ParentClass.ConstantPool[this._nameIndex] as ConstantUtf8Info;
                }
                return this._name;
            } 
        }
        public ConstantUtf8Info Description
        {
            get
            {
                if (this._description == null)
                {
                    this._description = this.ParentClass.ConstantPool[this._descriptorIndex] as ConstantUtf8Info;
                }
                return this._description;
            }
        }
        public UInt16 Index { get { return this._index; } }

        public LocalVariableInfo(BinaryReader reader,JavaClass cls):base(cls)
        {
            this._startPC = reader.ReadUInt16BE();
            this._length = reader.ReadUInt16BE();
            this._nameIndex = reader.ReadUInt16BE();
            this._descriptorIndex = reader.ReadUInt16BE();
            this._index = reader.ReadUInt16BE();
        }
    }
}
