﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public class ConstantInfo : ClassMember
    {

        private Byte _tag;

        public ConstantInfo(BinaryReader reader, JavaClass cls)
            : base(cls)
        {
            this._tag = reader.ReadByte();
        }

        public virtual Object GetValue()
        {
            throw new NotImplementedException();
        }

        public virtual void SetValue(Object value)
        {
            throw new NotImplementedException();
        }

    }
}
