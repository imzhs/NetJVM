﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetJavap
{
    public interface IWithAttributeMember
    {
        AttributeInfo[] Attributes { get; }
    }
}
